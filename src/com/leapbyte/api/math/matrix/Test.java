package com.leapbyte.api.math.matrix;

public class Test {

	public static void main(String[] args) {

		Matrix a = new Matrix(2, 2, new int [] { 
				 0,  1,  
				 2,  3
				});

		int d = a.determinant();

		Matrix b = new Matrix(2, 3, new int [] { 
				4, 5, 6,  
				7, 8, 9
				});

		Matrix x = a.multiply(b);
		x = x.createIdentityMatrix(3);

		Matrix matrix = new Matrix(3, 5, 5);

		matrix.setElement(1, 2, matrix.getElement(1, 2)-2);
		matrix.setElement(2, 2, matrix.getElement(2, 2)+2);

		matrix.toString();
		
		matrix = new Matrix(3, 3, new int [] { 
				1, 2, 3,  
				4, 5, 6,  
				7, 8, 9
				});

		matrix.add(matrix).scale(2).subtract(matrix).swapRows(0, 1).swapColumns(1, 2).transpose().isEqualsTo(matrix);

		int[] elements1d = { 
				0, 1, 0, 0, 7, 
				0, 0, 0, 0, 2,
				5, 5, -8, 4, 0
				};

		Matrix matrix1d  = new Matrix(3, 5, elements1d);

		int[][] elements2d = { 
				{0, 1, 0, 0, 3},
				{0, 3, 0, 0, 1} 
		};
		Matrix matrix2d  = new Matrix(elements2d);

		
		
/*
		Matrix identityMatrix  = Matrix.createIdentityMatrix(3, 5);
		System.out.println("identity."+identityMatrix.toString());

		matrix1.swapRows(1, 2);
		System.out.println("add."+matrix1.toString());

		int[] elements2 = { 
				0, 1, 0, 0, 7, 
				0, 0, 0, 0, 2,
				5, 5, -8, 4, 0
				};

		Matrix matrix2  = new Matrix(3, 5, elements2);
		System.out.println("add."+matrix2.toString());

		identityMatrix.equalsTo(matrix1);

		Matrix matrix3 = null;
		matrix3 = matrix1.add(matrix2);
		System.out.println("add."+matrix3.toString());

		matrix3 = matrix1.subtract(matrix2);
		System.out.println("sub." + matrix3.toString());

//		Matrix matrix0 = new Matrix(3, 5, 3);
//		Matrix matrix1 = new Matrix(3, 5, 2);
//		Matrix matrix2 = matrix0.add(matrix1);

//		int e = matrix.getElement(3, 5);
//		matrix.setElement(1, 5, e);
*/
	}

}
