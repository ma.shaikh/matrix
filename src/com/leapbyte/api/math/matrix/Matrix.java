package com.leapbyte.api.math.matrix;

import com.leapbyte.api.math.Fraction;

/**
 * @version 1.0
 * @author Mohammad Shaikh
 *
 * October 20th 2022
 * MATH-21002-002-202208 APPLIED LINEAR ALGEBRA 
 * Kent State University, Kent, OH.
 * 
 * This class is to model a Matrix object with matrix operations and some helper functions.
 * My intention is to write a class which is readable for clarity and can be use by my fellow 
 * students for their learning as well as to to use in other applications for rotating, shearing, 
 * etc a cube graphically. The main focus of this code is readability for beginners and not for 
 * optimize performance.
 * 
 *  
 * Note: 
 * First row and column index is 0 NOT 1;
 * [ 1 , 5, 8 ] is a [1x3] matrix which mean first element of the 
 * matrix is at the 0 row and 0 column, not [1,1]
 * 
 *  Warranty Disclaimer
 *  The information contained herein is subject to change without notice and is not warranted to 
 *  be error-free. If you find any errors, please report them to me in writing.
 *  
 */
public class Matrix {

	private static final Fraction ONE = new Fraction(1);
	private static final Fraction ZERO = new Fraction();

	private int rows;
	private int columns;
	private Fraction [] elements;

	public Matrix(int size) {
		this(size, size, ZERO);
	}

	public Matrix(int size, Fraction value) {
		this(size, size, value);
	}

	public Matrix(int rows, int columns, int value) {
		this(rows, columns, new Fraction(value));
	}

	public Matrix(int rows, int columns, Fraction value) {

		this.rows = rows;
		this.columns = columns;

		this.elements = new Fraction [rows*columns];
		for(int i=0; i<elements.length; i++) {
			this.elements[i] = value;
		}

		this.toString();
	}

	/**
	 * This constructor takes a 2d array and convert into 1d array
	 * @param elements
	 */
	public Matrix(int rows, int columns, int [] elements) {
		this(rows, columns, Fraction.toArray(elements));
	}

	public Matrix(int [][] elements) {
		this(elements.length, elements[0].length, Fraction.toArray(elements));
	}
/*
	public Matrix(Fraction [][] elements) {

		this.rows = elements.length;
		this.columns = elements[0].length;
		this.elements = new Fraction [(this.rows * this.columns)];
		
		for(int m=0; m<this.rows; m++) {
			for(int n=0; n<this.columns; n++) {
				this.elements[offset(m, n)] = elements[m][n];
			}
		}
		this.toString();
	}
*/
	public Matrix(int rows, int columns, Fraction [] elements) {

		this.rows = rows;
		this.columns = columns;

		if (elements.length != (rows * columns)) {
			throw new IllegalArgumentException("Number of elements must match the given order for the matrix");
		}

//		this.elements = elements;
		this.elements = new Fraction [elements.length];
		for(int i=0; i<elements.length; i++) {
			this.elements[i] = new Fraction(elements[i]);
		}

		this.toString();
	}

	public Matrix(Matrix matrix) {
		this(matrix.rows, matrix.columns, matrix.elements);
	}

	public Fraction [] getRow(int row) {
		return getRow(this, row);
	}

	public Fraction [] getRow(Matrix matrix, int row) {
		Fraction [] elements = new Fraction [matrix.columns];
		for(int i=0; i<elements.length; i++) {
			elements[i] = matrix.getElement(row, i);
		}
		return elements;
	}

	public void setRow(int row, Fraction [] elements) {
		setRow(this, elements, row);
	}

	public void setRow(Matrix a, Matrix b, int row) {
		setRow(this, b.getRow(row), row);
	}

	public void setRow(Matrix matrix, Fraction [] elements, int row) {
		for(int i=0; (i<elements.length && elements.length==matrix.columns); i++) {
			matrix.elements[offset(row, i)] = elements[i];
		}
	}

	public Fraction [] getColumn(int column) {
		return getColumn(this, column);
	}

	public Fraction [] getColumn(Matrix matrix, int column) {
		Fraction [] elements = new Fraction [matrix.rows];
		for(int i=0; i<elements.length; i++) {
			elements[i] = matrix.getElement(i, column);
		}
		return elements;
	}

	public void setColumn(Fraction [] elements, int column) {
		setColumn(this, elements, column);
	}

	public void setColumn(Matrix a, Matrix b, int column) {
		setColumn(this, b.getColumn(column), column);
	}

	public void setColumn(Matrix matrix, Fraction [] elements, int column) {
		for(int i=0; (i<elements.length && elements.length==matrix.rows); i++) {
			matrix.elements[offset(i, column)] = elements[i];
		}
	}


	public int getRows() {
		return rows;
	}
	
	public int getColumns() {
		return columns;
	}
	
	public Fraction [] getElements() {
		return elements;
	}

	public void setElement(int row, int column, Fraction value) {
		this.elements[offset(row, column)] = value;
	}

	public Fraction getElement(int row, int column) {
		return this.elements[offset(row, column)];
	}

	public int offset(int row, int column) {
		return offset(row, column, this.columns);
	}

	public int offset(int row, int column, int columns) {
		int i = (row * columns) + column;
		return i;
	}

	/**
	 * 
	 * swap 1nd rows by 2nd, matrix must have more than one row 
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public Matrix swapRows(int a, int b) {
		Matrix x = new Matrix(this);

		if((a<b && b<this.rows) || (b<a && a<this.rows)) {
			for(int i=0; i<x.columns; i++) {
				Fraction tmp = x.elements[offset(a, i)];
				x.elements[offset(a, i)] = x.elements[offset(b, i)];
				x.elements[offset(b, i)] = tmp;
			}
		}

		x.toString();

		return x;
	}

	/**
	 * 
	 * swap 1nd columns by 2nd, matrix must have more than one row 
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public Matrix swapColumns(int a, int b) {
		Matrix x = new Matrix(this);
		if((a<b && b<this.columns) || (b<a && a<this.columns)) {
			for(int i=0; i<x.rows; i++) {
				Fraction tmp = x.elements[offset(i, a)];
				x.elements[offset(i, a)] = x.elements[offset(i, b)];
				x.elements[offset(i, b)] = tmp;
			}
		}

		return x;
	}

	public Matrix add(Matrix b) {
		return add(this, b, +1);
	}

	public Matrix subtract(Matrix b) {
		return add(this, b, -1);
	}

	/**
	 * its just a convenient function
	 * @param n
	 * @return
	 */

	public Matrix add(Matrix a, Matrix b, int multiplier) {
		return add(a, b, 0, a.getRows(), 0, a.getColumns(), multiplier);
	}

	private Matrix add(Matrix a, Matrix b, int startRow, int endRow, int startCol, int endCol, int multiplier) {

		Matrix x = null;

		if((a.getElements().length == b.getElements().length) && (a.getRows() == b.getRows()) && (a.getColumns() == b.getColumns())) {
	
			x = new Matrix(a.getRows(), a.getColumns(), ZERO);
	
			for(int row=startRow; row<endRow; row++) {
				for(int col=startCol; col<endCol; col++) {
					a.setElement(row, col, (a.getElement(row, col).add(b.getElement(row, col).multiply(multiplier))));
				}
			}

			x.toString();
		}

		return x;
	}
	
	public Matrix multiply(Matrix b) {
		return multiply(this, b);
	}

	public Matrix multiply(Matrix a, Matrix b) {

		Matrix x = null;

		if(a.getColumns() == b.getRows()) {

			x = new Matrix(a.getRows(), b.getColumns(), ZERO);

			for(int i=0; i<b.getColumns(); i++) {

				for(int r=0; r<a.getRows(); r++) {

					for(int c=0; c<a.getColumns(); c++) {

						Fraction e = a.getElements()[offset(r, c, a.getColumns())];
						Fraction f = b.getElements()[offset(c, i, b.getColumns())];

						x.getElements()[offset(r, i, x.getColumns())].incrementByAdd(e.multiply(f));
					}

				}

			}

			x.toString();
		}

		return x;
	}

	/**
	 * Scaling a matrix
	 * @param multiplier
	 * @return
	 */
	public Matrix scale(int multiplier) {
		Matrix x = new Matrix(this);

		x = scale(x, 0, x.getRows(), 0, x.getColumns(), multiplier);

		return x;
	}

	public Matrix scaleRow(Matrix x, int row, int multiplier) {
		return scale(x, row, row, 0, x.getRows(), multiplier);
	}

	public Matrix scaleColumn(Matrix x, int col, int multiplier) {
		return scale(x, 0, x.getRows(), col, col, multiplier);
	}

	private Matrix scale(Matrix x, int startRow, int endRow, int startCol, int endCol, int multiplier) {

		for(int row=startRow; row<endRow; row++) {
			for(int col=startCol; col<endCol; col++) {
				x.elements[x.offset(row, col)].incrementByMultiply(new Fraction(multiplier));
			}
		}

		return x;
	}

	public void simplify(Matrix matrix) {

		Matrix a = new Matrix(2, 4, new int [] { 
				 6, 15, -12, 36,
				 4, 10, -8, -16
				});

		a.toString();
		

		Fraction [] row1 = a.getRow(0);
//		Fraction divisor1 = row1[0];
		for(int n=0; n<a.columns; n++) {
//			row1[n] /= divisor1;
		}
		a.setRow(0, row1);

		Fraction [] row2 = a.getRow(1);
//		Fraction divisor2 = (-1) * row2[0];
		for(int n=0; n<a.columns; n++) {

//			row2[n] += (row1[n] * divisor2);
		}
		a.setRow(1, row2);

		a.toString();
		
	}

	private Matrix pivot(int rowToScale, int rowToAdd, int multiplier) {
		Matrix x = new Matrix(this);
		x.scaleRow(x, rowToScale, multiplier);
		return x;
	}

	private Matrix inverse() {
		Matrix x = new Matrix(this);

		return x;
	}

	private void solve() {
		// TODO Auto-generated method stub

	}

	private void opposite() {
		// TODO Auto-generated method stub

	}

	/**
	 * Transposing a matrix
	 * 
	 * @return
	 */
	public Matrix transpose() {
		Matrix x = null;

		if(isSquareMatrix()) {
			x = new Matrix(this.columns, this.rows, ZERO);
			for(int m=0; m<this.rows; m++) {
				for(int n=0; n<this.columns; n++) {
					x.setElement(n, m, this.getElement(m, n));
				}
			}
			x.toString();
		}

		return x;
	}

	public Fraction determinant(Matrix a) {

		Fraction determinant = ZERO;

		if(a.isSquareMatrixOfSize(1)) {
			determinant = determinant1by1(a);
		} else 
		if(a.isSquareMatrixOfSize(2)) {
			determinant = determinant2by2(a);
		} else 
		if(a.isSquareMatrixOfSize(3)) {
			determinant = determinant3by3(a);
		} else {
			throw new IllegalArgumentException("Determinant cannot be calculated for Matrix larger than 3x3 matrix. Not implemented!");
		}

		return determinant;
	}

	public Fraction determinant1by1(Matrix a) {
		Fraction determinant = ZERO;
		if(a.isSquareMatrixOfSize(1)) {
			determinant = a.getElement(0, 0);
		}
		return determinant;
	}


	public Fraction determinant2by2(Matrix a) {
		Fraction determinant = ZERO;
		if(a.isSquareMatrixOfSize(2)) {
			Fraction ad = (a.getElement(0, 0).multiply(a.getElement(1, 1)));
			Fraction bc = (a.getElement(0, 1).multiply(a.getElement(0, 1)));
			determinant = (ad.subtract(bc));
		}
		return determinant;
	}


	public Fraction determinant3by3(Matrix a) {
		Fraction determinant = ZERO;

		if(a.isSquareMatrixOfSize(3)) {
			Matrix x1 = new Matrix(2, 2, new Fraction [] { 
					 a.getElement(1, 1),  a.getElement(1, 2),  
					 a.getElement(2, 1),  a.getElement(2, 2)
					});
	
//			int coFactor1 = ((-1)^(1+1) * determinant2by2(x1));

	
			Matrix x2 = new Matrix(2, 2, new Fraction [] { 
					 a.getElement(1, 0),  a.getElement(1, 2),  
					 a.getElement(2, 0),  a.getElement(2, 2)
					});
	
//			int coFactor2 = ((-1)^(1+2) * determinant2by2(x2));

	
			Matrix x3 = new Matrix(2, 2, new Fraction [] { 
					 a.getElement(1, 0),  a.getElement(1, 1),  
					 a.getElement(2, 0),  a.getElement(1, 2)
					});

//			int coFactor3 = ((-1)^(1+3) * determinant2by2(x3));
			
//			determinant = coFactor1 + coFactor2 + coFactor3;
		}

		return determinant;
	}

	public Fraction determinant() {
		return determinant(this);
	}

	public Fraction determinant1by1() {
		return determinant(this);
	}

	public Fraction determinant2by2() {
		return determinant2by2(this);
	}

	public Fraction determinant3by3() {
		return determinant3by3(this);
	}

	/*
	 * 
	 * 
	 */
	
	public boolean isSquareMatrix() {
		return isSquareMatrix(this);
	}

	public boolean isSquareMatrixOfSize(int size) {
		return isSquareMatrix(this, size);
	}

	public boolean isSquareMatrixOfSize(Matrix a, int size) {
		return (isSquareMatrix(a) && a.rows == size);
	}

	public boolean isSymmetricMatrix() {
		return isSymmetricTo(this, this);
	}

	public boolean isSymmetricTo(Matrix b) {
		return isSymmetricTo(this, b);
	}
	
	public boolean isInverseOf(Matrix b) {
		return isInverseOf(this, b);
	}

	public boolean isEqualsTo(Matrix b) {
		return isEqualsTo(this, b);
	}

	public boolean isSkewSymmetric() {
		return (this.isAllDiagonalElementsZero() && createSkewSymmetricMatrix(this).isEqualsTo(this));
	}

	public boolean isSkewSymmetric(Matrix a) {
		return (a.isAllDiagonalElementsZero() && createSkewSymmetricMatrix(a).isEqualsTo(a));
	}

	public boolean isAllDiagonalElementsZero() {
		return isAllDiagonalElementsHave(this, ZERO);
	}

	public boolean isSymmetricTo(Matrix a, Matrix b) {

		Matrix x = b.transpose();
		if(x != null) {
			return a.equals(x);
		}

		return false;
	}


	public boolean isSquareMatrix(Matrix a, int size) {
		return (isSquareMatrix(a) && a.getRows() == size);
	}


	public boolean isSquareMatrix(Matrix a) {
		return ((a.getRows()>0) && (a.getRows() == a.getColumns()) && ((a.getRows() * a.getColumns()) == a.getElements().length));
	}
	

	public boolean isAllDiagonalElementsHave(Matrix a, Fraction value) {

		boolean is = false;

		if(a.isSquareMatrix()) {
			Fraction v = ZERO;
			for(int i=0; i<a.getColumns(); i++) {
				v.incrementByAdd(a.getElement(i, i));
			}
			is = (v.isEqualsTo(0) && a.getColumns()>0);
		}

		return is;
	}

	public boolean isInverseOf(Matrix a, Matrix b) {
		return multiply(a, b).isEqualsTo(createIdentityMatrix(a.getColumns()));
	}

	/**
	 * comparing two matrices if matrix order and all element values are identical
	 * @param b
	 * @return
	 */

	public boolean isEqualsTo(Matrix a, Matrix b) {

		boolean isEquals = (a.getRows() == b.getRows() && a.getColumns() == b.getColumns());
		if (a.getElements().length > 0) {
			for (int i = 0; i < a.getElements().length; i++) {
				isEquals =  (a.getElements()[i] == b.getElements()[i]);
				if(!isEquals) {
					break;
				}

			}
		}

		a.toString();

		if(isEquals) {
			System.out.println("is equals to");
		} else {
			System.out.println("is not equals to");
		};

		b.toString();

		return isEquals;
	}

	public Matrix createUnitMatrix(int size) {
		return createIdentityMatrix(size);
	}

	public Matrix createSkewSymmetricMatrix(Matrix a) {
		return a.transpose().scale(-1);
	}

	public Matrix createIdentityMatrix(int size) {
		return createSquareMatrixAndInitializeDiagonalElements(size, ONE);
	}

	public Matrix createSquareMatrix(int size) {
		return new Matrix(size, ZERO);
	}

	public Matrix createSquareMatrixAndInitializeDiagonalElements(int size, Fraction value) {

		Matrix x = createSquareMatrix(size);

		for(int i=0; i<x.getColumns(); i++) {
			x.setElement(i, i, value);
		}

		return x;
	}

	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();

		sb.append("[" + this.rows + "x" + this.columns + "] = ");

		for (int m = 0; m < this.rows; m++) {
			sb.append("\n[");
			for (int n = 0; n < this.columns; n++) {

				int i = offset(m, n);

				sb.append(" " + this.elements[i] + " ");
			}
			sb.append("]");
		}

		// comment it out
		System.out.println(sb.toString());

		return sb.toString();
	}
	
	private void isUpperTriagonal() {
		// TODO Auto-generated method stub

	}
	
	private void isLowerTriagonal() {
		// TODO Auto-generated method stub

	}
	
	private void isPivotColumn(int i) {
		// TODO Auto-generated method stub
	}

}
